//
//  ViewController.swift
//  testnew
//
//  Created by Mac on 12/16/18.
//  Copyright © 2018 WebTech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let result = checkDuplicates(withArray:["GRK","YPR","SSV","GRK","GRK","YPR","YPR","YPR","GRK"] )
        print(result)
        
        let holes = checkHoles(withNumber: 8800)
        print(holes)
        
        let getMax = checkDecks(withMarDeck: [9,5,6], withAndDeck: [4,15,16,7], withStart: "Even")
        print(getMax)

        
    }
    
     func checkDuplicates(withArray:Array<String>) -> String {
        let dictArray = withArray.reduce(into:[:]){
            $0[$1,default:0] += 1
        }
        let filterdArray = dictArray.filter{
            $0.value >= dictArray.values.max()!
        }
            .sorted {(E1,E2)-> Bool in
                return E1.key < E2.key
        }
    
        return (filterdArray.last?.key)!
    }
    
    func checkHoles(withNumber:Int) -> Int {
    let set1 = ["1","2","3","5","7"]
        let set2 = ["4","6","9","0"]
        let set3 = ["8"]
        
        let holes = String(withNumber)
            .compactMap{
                return String($0)
        }
        
        let holesArray = holes.map{(item:String)-> Int in
                var holeCount = 0
            if set1.contains(item) {holeCount = 0} else if set2.contains(item) {holeCount = 1} else if set3.contains(item) {holeCount = 2}
            return holeCount
        }
        return holesArray.reduce(0,+)
    }
    
    
    func checkDecks(withMarDeck:[Int],withAndDeck:[Int],withStart:String) -> String {

        var dict = [Int:Int]()
        let marModified = withMarDeck.enumerated().map{(index,element) in
            return withStart == "Even" ? (index % 2 == 0 ? element:nil) : (index % 2 == 0 ? nil:element)
        }
            .compactMap{
                return $0
        }
        let andModified = withAndDeck.enumerated().map{(index,element) in
            return withStart == "Even" ? (index % 2 == 0 ? element:nil) : (index % 2 == 0 ? element:nil)
        }
            .compactMap{
                return $0
        }
        
        
        zip(marModified, andModified)
            .forEach{
                dict[$0] = $1
        }
        
        let marValue = dict.reduce(0) {(sum,element)->Int in
            return sum + element.key - element.value
        }
        let andValue = dict.reduce(0) {(sum,element)->Int in
            return sum + element.value - element.key
        }
        
        return marValue > andValue ? "Maria" : (marValue == andValue ? "Tie" : "Andrea")
    }
    
    
//    func checkDuplicates(withArray:Array<String>) -> String {
//        let dictArray = withArray.reduce(into:[:]) {
//            $0[$1,default:0] += 1
//        }
//        print(dictArray)
//
//        let filterdArray = dictArray.filter{
//            $0.value >= dictArray.values.max()!
//        }
//            .sorted {(E1,E2) -> Bool in
//                return E1.key < E2.key
//        }
//        print(filterdArray)
//
//        return (filterdArray.last?.key)!
//    }
    
//    func checkHoles(withNumber:Int) -> Int {
//
//        let set1 = ["1","2","3","5","7"]
//         let set2 = ["4","6","9","0"]
//        let set3 = ["8"]
//
//        let holes = String(withNumber)
//            .compactMap{
//                return String($0)
//        }
//        print(holes)
//
//        let holesArray = holes.map{ (item:String) -> Int in
//            var holecount = 0
//            if set1.contains(item) {holecount = 0} else if set2.contains(item) {holecount = 1} else if set3.contains(item) {holecount = 2}
//            return holecount
//        }
//
//        print(holesArray)
//
//        return holesArray.reduce(0,+)
//    }
    
    
    
//    func checkDecks(withMarDeck:[Int],withAndDeck:[Int],withStart:String) -> String {
//        var dict = [Int:Int]()
//
//        let modifiedMaria = withMarDeck.enumerated().map{(index,element) in
//            return withStart == "Even" ? (index % 2 == 0 ? element:nil) : (index % 2 == 0 ? nil:element)
//        }
//            .compactMap{
//                return $0
//        }
//        let modifiedAndrea = withAndDeck.enumerated().map{(index,element) in
//            return withStart == "Even" ? (index % 2 == 0 ? element:nil) : (index % 2 == 0 ? nil:element)
//            }
//            .compactMap{
//                return $0
//        }
//        print(modifiedMaria)
//        print(modifiedAndrea)
//
//        zip(modifiedMaria, modifiedAndrea)
//            .forEach{
//                dict[$0] = $1
//        }
//
//        print(dict)
//
//        let mariaValue = dict.reduce(0) {(sum,element)->Int in
//           return sum + element.key - element.value
//        }
//
//        let andreaValue = dict.reduce(0) {(sum,element)->Int in
//            return sum + element.value - element.key
//        }
//
//        print(mariaValue)
//        print(andreaValue)
//
//
//        return mariaValue > andreaValue ? "Maria" : (mariaValue == andreaValue ? "Tie" : "Andrea")
//    }


}

